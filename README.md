# sasw-websocket-tester

Simple websocket endpoint for testing purposes. It accepts and keeps connections in memory in a thread-safe dictionary, it responses with a unique connection ID per session
whenever something is received through the websocket at server side, and it has an http connections endpoint that can be used to push
notifications to any websocket client.

## How to use

### Connect to websocket
```
wscat -c ws://localhost:5000/ws
```

Every message sent to the websocket, once connected, will receive a payload specifying the unique connection Id for the websocket established
```
Connected (press CTRL+C to quit)
> foo
< {"statusCode":200,"connectionId":"klHkqyncJA"}
```

### Send websocket notifications
Any payload can be sent to a specific websocket provided its connection Id is known. To do so, send an http post request with the json payload to the `@connections` endpoint on the specific connection Id. Example:
```
curl -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" -X POST http://localhost:5000/@connections/klHkqyncJA
```

## How to use as a docker container
IMPORTANT: The docker image exposes port `8080` and it adds the `ENV ASPNETCORE_URLS=http://*:8080` so that this is the port AspNetCore is listening on.

This app is stored as a docker image in a private repository, so login Docker into GitLab repository
```
docker login registry.gitlab.com -u {{deploy_token_name_or_username}} -p {{deploy_token_or_password}}
```

Run the container by mapping it to the desired localhost port (e.g: `5000` or any other)

```
docker run -p 5000:8080 registry.gitlab.com/sunnyatticsoftware/sasw-community/sasw-websocket-tester:latest
```

Then use normally but under the mapped hostname (e.g: `ws://localhost:5000/ws` and `http://localhost:5000`)
