using Microsoft.AspNetCore.Builder;
using Sasw.WebsocketTester.Application.Factories;
using Sasw.WebsocketTester.Application.Repositories;
using Sasw.WebsocketTester.Application.Services;
using Sasw.WebsocketTester.Middlewares;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

// IoCC
var services = builder.Services;

services
    .AddSingleton<InMemorySubscriptionRepository>()
    .AddSingleton<ConnectionIdFactory>()
    .AddTransient<ConnectionService>()
    .AddTransient<Notificator>()
    .AddControllers();

var app = builder.Build();

// Http Pipeline
var websocketOptions =
    new WebSocketOptions
    {
        KeepAliveInterval = TimeSpan.FromMinutes(5)
    };

app
    .UseRouting()
    .UseMiddleware<InfoMiddleware>()
    .UseWebSockets(websocketOptions)
    .UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });

app.Run();