namespace Sasw.WebsocketTester.Middlewares;

public class InfoMiddleware
{
    private readonly RequestDelegate _next;

    public InfoMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        if (httpContext.Request.Path == "/")
        {
            var host = httpContext.Request.Host;
            await httpContext.Response.WriteAsync($"Connect to ws://{host}/ws");
            return;
        }
        
        await _next(httpContext);
    }
}