using Microsoft.AspNetCore.Mvc;
using Sasw.WebsocketTester.Application.Services;
using System.Net.WebSockets;

namespace Sasw.WebsocketTester.Controllers;

public class WebsocketController
    : ControllerBase
{
    private readonly ConnectionService _connectionService;
    private readonly Notificator _notificator;

    public WebsocketController(
        ConnectionService connectionService,
        Notificator notificator)
    {
        _connectionService = connectionService;
        _notificator = notificator;
    }
    
    [HttpGet("/ws")]
    public async Task GetWebsocketConnection()
    {
        if (HttpContext.WebSockets.IsWebSocketRequest)
        {
            using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();
            var connectionId = await _connectionService.CreateConnection(webSocket);
            await KeepOpen(connectionId, webSocket);
            await _connectionService.DeleteConnection(connectionId);
        }
        else
        {
            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        }
    }

    private async Task KeepOpen(string connectionId, WebSocket webSocket)
    {
        var buffer = new byte[1024 * 4];
        var receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
        while (!receiveResult.CloseStatus.HasValue)
        {
            await _notificator.Pong(connectionId);
            var bufferSegment = new ArraySegment<byte>(buffer);
            receiveResult = await webSocket.ReceiveAsync(bufferSegment, CancellationToken.None);
        }
        await webSocket.CloseAsync(
            receiveResult.CloseStatus.Value,
            receiveResult.CloseStatusDescription,
            CancellationToken.None);
    }
}