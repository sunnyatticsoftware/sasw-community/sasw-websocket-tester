﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace Sasw.WebsocketTester.Controllers;

[Route("[controller]")]
[AllowAnonymous]
public class HealthController
    : ControllerBase
{
    private readonly IWebHostEnvironment _environment;

    public HealthController(IWebHostEnvironment environment)
    {
        _environment = environment;
    }
        
    /// <summary>
    /// Gets service's health
    /// </summary>
    [HttpGet]
    public IActionResult Get()
    {
        var environmentName = _environment.EnvironmentName;
        var currentDateTime = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
        var assemblyName = typeof(Program).Assembly.GetName();
        var result = $"Assembly={assemblyName}, Environment={environmentName}, CurrentTime={currentDateTime}";
        return Ok(result);
    }
}