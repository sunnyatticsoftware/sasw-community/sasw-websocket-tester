using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sasw.WebsocketTester.Application.Services;

namespace Sasw.WebsocketTester.Controllers;

[Route("@connections")]
[AllowAnonymous]
public class ConnectionsController
    : ControllerBase
{
    private readonly Notificator _notificator;

    public ConnectionsController(Notificator notificator)
    {
        _notificator = notificator;
    }
    
    [HttpPost("{connectionId}")]
    public async Task<IActionResult> Notify(string connectionId, [FromBody] object notification)
    {
        await _notificator.Notify(connectionId, notification);
        return Ok();
    }
}