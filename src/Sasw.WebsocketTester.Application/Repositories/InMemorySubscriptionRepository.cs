using System.Collections.Concurrent;
using System.Net.WebSockets;

namespace Sasw.WebsocketTester.Application.Repositories;

public sealed class InMemorySubscriptionRepository
{
    private readonly IDictionary<string, WebSocket> _connectionIds = new ConcurrentDictionary<string, WebSocket>();

    public void AddConnection(string connectionId, WebSocket webSocket)
    {
        var isAdded = _connectionIds.TryAdd(connectionId, webSocket);
        if (!isAdded)
        {
            throw new Exception($"Could not add websocket to connection Id {webSocket}");
        }
    }

    public bool Delete(string connectionId)
    {
        return _connectionIds.Remove(connectionId);
    }

    public WebSocket GetWebSocket(string connectionId)
    {
        var isExisting = _connectionIds.TryGetValue(connectionId, out var webSocket);
        if (!isExisting || webSocket is null)
        {
            throw new KeyNotFoundException($"Could not find websocket for connection Id {connectionId}");
        }

        return webSocket;
    }
}