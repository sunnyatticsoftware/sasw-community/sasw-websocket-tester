using Sasw.WebsocketTester.Application.Models;
using Sasw.WebsocketTester.Application.Repositories;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;

namespace Sasw.WebsocketTester.Application.Services;

public sealed class Notificator
{
    private readonly InMemorySubscriptionRepository _inMemorySubscriptionRepository;

    public Notificator(InMemorySubscriptionRepository inMemorySubscriptionRepository)
    {
        _inMemorySubscriptionRepository = inMemorySubscriptionRepository;
    }
    
    public async Task Notify(
        string connectionId, 
        object notification, 
        CancellationToken cancellationToken = new CancellationToken())
    {
        var websocket = _inMemorySubscriptionRepository.GetWebSocket(connectionId);
        
        var responseJson = JsonSerializer.Serialize(notification, new JsonSerializerOptions(JsonSerializerDefaults.Web));
        var responseJsonBytes = Encoding.UTF8.GetBytes(responseJson);
        
        await websocket.SendAsync(
            new ArraySegment<byte>(responseJsonBytes, 0, responseJsonBytes.Length),
            WebSocketMessageType.Text,
            true,
            CancellationToken.None);
    }

    public async Task Pong(string connectionId)
    {
        var pong =
            new WebSocketPong
            {
                StatusCode = 200,
                ConnectionId = connectionId
            };
        
        await Notify(connectionId, pong);
    }
}