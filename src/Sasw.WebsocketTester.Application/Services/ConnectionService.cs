using Sasw.WebsocketTester.Application.Factories;
using Sasw.WebsocketTester.Application.Repositories;
using System.Net.WebSockets;

namespace Sasw.WebsocketTester.Application.Services;

public sealed class ConnectionService
{
    private readonly ConnectionIdFactory _connectionIdFactory;
    private readonly InMemorySubscriptionRepository _inMemorySubscriptionRepository;

    public ConnectionService(
        ConnectionIdFactory connectionIdFactory,
        InMemorySubscriptionRepository inMemorySubscriptionRepository)
    {
        _connectionIdFactory = connectionIdFactory;
        _inMemorySubscriptionRepository = inMemorySubscriptionRepository;
    }

    public Task<string> CreateConnection(WebSocket webSocket)
    {
        var connectionId = _connectionIdFactory.Create();
        _inMemorySubscriptionRepository.AddConnection(connectionId, webSocket);
        return Task.FromResult(connectionId);
    }

    public Task DeleteConnection(string connectionId)
    {
        _ = _inMemorySubscriptionRepository.Delete(connectionId);
        return Task.CompletedTask;
    }
}