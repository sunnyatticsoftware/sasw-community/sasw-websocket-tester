using System.Diagnostics.CodeAnalysis;

namespace Sasw.WebsocketTester.Application.Factories;

[SuppressMessage("ReSharper", "StringLiteralTypo")]
public sealed class ConnectionIdFactory
{
    public string Create()
    {
        var random = new Random();
        const int connectionIdLength = 10;
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return new string(Enumerable.Repeat(chars, connectionIdLength)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }
}