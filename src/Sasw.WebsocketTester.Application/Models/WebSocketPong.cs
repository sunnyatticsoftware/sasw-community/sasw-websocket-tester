namespace Sasw.WebsocketTester.Application.Models;

public sealed class WebSocketPong
{
    public int StatusCode { get; init; }
    public string ConnectionId { get; init; } = string.Empty;
}